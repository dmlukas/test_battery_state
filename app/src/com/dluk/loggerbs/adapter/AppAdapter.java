package com.dluk.loggerbs.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.dluk.loggerbs.R;
import com.dluk.loggerbs.db.BatteryStateTable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Dzmitry Lukashanets on 03.07.2014.
 */
public class AppAdapter extends CursorAdapter {

    private final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());

    public AppAdapter(Context context, Cursor c) {
        super(context, c, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        View view = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        holder.tvTime = (TextView) view.findViewById(R.id.item_tv_time);
        holder.tvState = (TextView) view.findViewById(R.id.item_tv_state);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder holder = (ViewHolder) view.getTag();
        long time = cursor.getLong(cursor.getColumnIndex(BatteryStateTable.TIME));
        int state = cursor.getInt(cursor.getColumnIndex(BatteryStateTable.STATE));
        holder.tvTime.setText(SDF.format(new Date(time)));
        holder.tvState.setText(String.valueOf(state) + "%");
    }

    private static class ViewHolder {
        TextView tvTime;
        TextView tvState;
    }
}
