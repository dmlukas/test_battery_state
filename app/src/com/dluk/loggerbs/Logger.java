package com.dluk.loggerbs;

import java.util.ArrayList;

public class Logger {

    private static final String PREFIX = "Logger-BatteryState";
    private static final ArrayList<String> list = new ArrayList<>();

    //add class, for show logs info and debug
    static {
        list.trimToSize();
    }

    private final String className;
    private final String prefix;
    private boolean isEnabled;


    private Logger(String name, String prefix) {
        int i = name.lastIndexOf('.');
        className = name.substring(i + 1);
        this.prefix = prefix;

        if (list.isEmpty() || list.contains(name)) {
            isEnabled = true;
        }
    }

    public static Logger getLogger(Class<?> class1) {
        return getLogger(class1, null);
    }

    public static Logger getLogger(Class<?> class1, String prefix) {
        if (class1 == null)
            throw new NullPointerException();
        if (prefix == null) {
            prefix = PREFIX;
        } else {
            prefix = prefix + "_";
        }
        return new Logger(class1.getSimpleName(), prefix);
    }

    public void debug(String msg) {
        if (BuildConfig.DEBUG && isEnabled)
            android.util.Log.d(className + ": " + getThread(), "[" + msg + "]");
    }

    public void warning(String msg) {
        if (BuildConfig.DEBUG)
            android.util.Log.w(className + ": " + getThread(), "[" + msg + "]");
    }

    public void info(String msg) {
        if (BuildConfig.DEBUG && isEnabled)
            android.util.Log.i(className + ": " + getThread(), "[" + msg + "]");
    }

    public void error(String msg) {
        if (BuildConfig.DEBUG)
            android.util.Log.e(className + ": " + getThread(), "[" + msg + "]");
    }

    private String getThread() {
        return Thread.currentThread().getName();
    }

//    public static final class BugsnagSender {
//
//        public static void register(Context context) {
//            Bugsnag.register(context, SettingsApp.BUGSNAG_KEY);
//        }
//
//        public static void setContext(String context) {
//            Bugsnag.setContext(context);
//        }
//
//        public static void setUser(String id, String email, String user) {
//            Bugsnag.setUser(id, email, user);
//        }
//
//        public static void setReleaseStage(String releaseStage) {
//            Bugsnag.setReleaseStage(releaseStage);
//        }
//
//        public static void notify(Throwable e) {
//            Bugsnag.notify(e);
//        }
//
//        public static void notifyWithMetaData(Throwable e, ArrayList<BugsnagTab> arrayList) {
//
//            MetaData metaData = new MetaData();
//
//            for (BugsnagTab bugsnagTab : arrayList) {
//                metaData.addToTab(bugsnagTab.getArg1(), bugsnagTab.getArg2(), bugsnagTab.getArg3());
//            }
//            Bugsnag.notify(e, metaData);
//        }
//
//        public static void setNotifyReleaseStages(final String... notifyReleaseStages) {
//            //"production", "development", "testing"
//            Bugsnag.setNotifyReleaseStages(notifyReleaseStages);
//        }
//
//        public static void setAutoNotify(final boolean autoNotify) {
//            Bugsnag.setAutoNotify(autoNotify);
//        }
//
//        public static void setFilters(final String... filters) {
//            //new String[]{"password", "credit_card_number"}
//            Bugsnag.setFilters(filters);
//        }
//
//        public static void setProjectPackages(final String... projectPackages) {
//            Bugsnag.setProjectPackages(projectPackages);
//        }
//
//        public static void setIgnoreClasses(final String... ignoreClasses) {
//            Bugsnag.setIgnoreClasses(ignoreClasses);
//        }
//
//        public static void setAppVersion(final String appVersion) {
//            Bugsnag.setAppVersion(appVersion);
//        }
//    }
}
