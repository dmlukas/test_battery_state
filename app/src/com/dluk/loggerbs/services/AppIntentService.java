package com.dluk.loggerbs.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;

import com.dluk.loggerbs.Logger;
import com.dluk.loggerbs.Settings;
import com.dluk.loggerbs.db.BatteryStateTable;

/**
 * Created by Dzmitry Lukashanets on 02.07.2014.
 */
public class AppIntentService extends IntentService {

    private static final Logger LOG = Logger.getLogger(AppIntentService.class);

    public static final String ACTION_GET_INFO = "action.get.info";
    public static final String ACTION_CLEAN_INFO = "action.clean.info";


    public static void sendAction(Context context, String action) {
        context.startService(new Intent(context, AppIntentService.class).setAction(action));
        LOG.info("send action - " + action);
    }

    public AppIntentService() {
        super("AppIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            switch (intent.getAction()) {

                case ACTION_CLEAN_INFO:
                    BatteryStateTable.deleteAll(getApplicationContext());
                    break;

                case ACTION_GET_INFO:
                    Intent batteryStatus = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
                    BatteryStateTable.addState(getApplicationContext(), batteryStatus);
                    setNotify();
                    break;

                default:
                    break;
            }
        }
    }

    private void setNotify() {
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AppIntentService.class).setAction(ACTION_GET_INFO);
        PendingIntent pIntent = PendingIntent.getService(getApplication(), 0, intent, 0);
        am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + Settings.TIME_OUT_REPEAT_GET_INFO, pIntent);
    }
}
