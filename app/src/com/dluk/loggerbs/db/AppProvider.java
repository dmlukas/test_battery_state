package com.dluk.loggerbs.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.dluk.loggerbs.Logger;

/**
 * Created by Dzmitry Lukashanets on 02.07.2014.
 */
public class AppProvider extends ContentProvider {

    private static final Logger LOG = Logger.getLogger(AppProvider.class);
    public static final String AUTHORITY = AppProvider.class.getName();

    private static final int BATTERY_STATE = 1;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BatteryStateTable.TABLE_NAME, BATTERY_STATE);
    }

    private AppSQLiteHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new AppSQLiteHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        LOG.info("query: " + uri.toString());
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setDistinct(false);

        switch (sURIMatcher.match(uri)) {

            case BATTERY_STATE:
                queryBuilder.setTables(BatteryStateTable.TABLE_NAME);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        Cursor cursor = queryBuilder.query(dbHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID;
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        switch (sURIMatcher.match(uri)) {

            case BATTERY_STATE:
                rowID = db.insert(BatteryStateTable.TABLE_NAME, null, values);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        Uri resultUri = ContentUris.withAppendedId(uri, rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        LOG.info("insert uri: " + resultUri.toString());
        return resultUri;

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        String tableName;

        switch (sURIMatcher.match(uri)) {

            case BATTERY_STATE:
                tableName = BatteryStateTable.TABLE_NAME;
                break;

            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rowsDeleted = db.delete(tableName, selection, selectionArgs);
        LOG.info("delete from " + tableName + ", number row = " + rowsDeleted);
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String tableName;

        switch (sURIMatcher.match(uri)) {

            case BATTERY_STATE:
                tableName = BatteryStateTable.TABLE_NAME;
                if (TextUtils.isEmpty(selection)) {
                    selection = BatteryStateTable._ID + "=" + uri.getLastPathSegment();
                } else {
                    selection = selection + " AND " + BatteryStateTable._ID + "=" + uri.getLastPathSegment();
                }
                break;


            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count = db.update(tableName, values, selection, selectionArgs);
        LOG.info("update table " + tableName + " row = " + count);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
