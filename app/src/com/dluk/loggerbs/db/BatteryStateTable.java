package com.dluk.loggerbs.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Looper;
import android.provider.BaseColumns;

import com.dluk.loggerbs.BuildConfig;

/**
 * Created by Dzmitry Lukashanets on 02.07.2014.
 */
public class BatteryStateTable implements BaseColumns {

    public static final String TABLE_NAME = "BATTERY_STATE";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AppProvider.AUTHORITY + "/" + TABLE_NAME);
    public static final String TIME = "time";
    public static final String STATE = "state";

    public static final String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TIME + " INTEGER, " + STATE + " INTEGER);";

    public static Uri addState(Context context, Intent intent) {
        ensureNotOnMainThread();
        ContentValues cv = new ContentValues();

        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = level * 100 / (float) scale;
        cv.put(TIME, System.currentTimeMillis());
        cv.put(STATE, Math.round(batteryPct));
        return context.getContentResolver().insert(CONTENT_URI, cv);
    }

    public static int deleteAll(Context context) {
        ensureNotOnMainThread();
        return context.getContentResolver().delete(CONTENT_URI, null, null);
    }


    private static void ensureNotOnMainThread() {

        if (BuildConfig.DEBUG) {
            if (Looper.myLooper() != Looper.getMainLooper())
                return;
            throw new IllegalStateException("This method cannot be called from the UI thread.");
        }
    }
}
