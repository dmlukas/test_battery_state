
package com.dluk.loggerbs.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.LinkedHashMap;

class AppSQLiteHelper extends SQLiteOpenHelper {

    private final static String DB_NAME = "BatteryState.db";
    private static final int DB_VERSION = 1;

    private static final LinkedHashMap<String, String> tablesMap = new LinkedHashMap<>();

    static {
        tablesMap.put(BatteryStateTable.TABLE_NAME, BatteryStateTable.TABLE_CREATE);
    }

    public AppSQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        for (String tableCreate : tablesMap.values()) {
            Log.i(AppSQLiteHelper.class.getName(),tableCreate);
            db.execSQL(tableCreate);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        for (String tableName : tablesMap.keySet()) {
            db.execSQL("DROP TABLE IF EXISTS " + tableName);
        }
        onCreate(db);
    }

}
