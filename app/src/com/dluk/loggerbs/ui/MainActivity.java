package com.dluk.loggerbs.ui;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ListView;

import com.dluk.loggerbs.Logger;
import com.dluk.loggerbs.R;
import com.dluk.loggerbs.adapter.AppAdapter;
import com.dluk.loggerbs.db.BatteryStateTable;
import com.dluk.loggerbs.services.AppIntentService;

/**
 * Created by Dzmitry Lukashanets on 02.07.2014.
 */
public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {

    private static final Logger LOG = Logger.getLogger(MainActivity.class);
    private static final String PREF_FIRST_LAUNCH = "pref.first.launch";
    private static final int LOADER_BATTERY_STATE = 1;
    private AppAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        boolean firstLaunch = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(PREF_FIRST_LAUNCH, true);

        if (firstLaunch) {
            AppIntentService.sendAction(getApplication(), AppIntentService.ACTION_GET_INFO);
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putBoolean(PREF_FIRST_LAUNCH, false).commit();
        }

        adapter = new AppAdapter(getApplicationContext(), null);
        ((ListView) findViewById(R.id.main_lv)).setAdapter(adapter);
        findViewById(R.id.btn_clean).setOnClickListener(this);

        getLoaderManager().restartLoader(LOADER_BATTERY_STATE, null, this);
    }

    @Override
    protected void onDestroy() {
        getLoaderManager().destroyLoader(LOADER_BATTERY_STATE);
        super.onDestroy();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getApplicationContext(), BatteryStateTable.CONTENT_URI, null, null, null, BatteryStateTable.TIME + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            adapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_clean:
                AppIntentService.sendAction(getApplicationContext(), AppIntentService.ACTION_CLEAN_INFO);
                break;
        }

    }
}
