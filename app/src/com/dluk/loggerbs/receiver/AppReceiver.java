package com.dluk.loggerbs.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dluk.loggerbs.services.AppIntentService;

public class AppReceiver extends BroadcastReceiver {

    public AppReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        AppIntentService.sendAction(context, AppIntentService.ACTION_GET_INFO);
    }
}
